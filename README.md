An authoritative strategy for multiplayer game physics.

This project includes the following:

An entity spawning and updating implementation.

High quality player controllers for both a 3rd person character and Ship.

Handlers for on request physics events.

This is for use in the following system architecture.
https://i.imgur.com/T17iAIc.png