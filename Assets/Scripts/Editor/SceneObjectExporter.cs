﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;
using System;
using System.IO;

namespace Espial.ChunkManagement
{
    public class SceneObjectExporter : EditorWindow
    {

        Scene scene;

        public void Awake()
        {
            scene = EditorSceneManager.GetActiveScene();
        }

        [MenuItem("Espial/Process scene for physics")]
        public static void ShowWindow()
        {
            GetWindow(typeof(SceneObjectExporter));
        }

        void OnGUI()
        {
            if (GUILayout.Button("Count"))
            {
                CountObjects();
            }

            if (GUILayout.Button("Assign Compnents By Names"))
            {
                AssignComponents();
            }
        }

        private void AssignComponents()
        {
            List<GameObject> objects = new List<GameObject>();
            objects.AddRange(UnityEngine.Object.FindObjectsOfType<GameObject>());

            for (int i = 0; i < objects.Count; i++)
            {
                GameObject currentobject = objects[i];
                if (currentobject.activeInHierarchy)
                {
                    string tag = currentobject.tag.ToLower();
                    if (tag.Contains("tree"))
                    {
                        //DestroyImmediate(currentobject.GetComponent<Resource>());
                        Resource resource = currentobject.AddComponent<Resource>();
                        resource.requirement = 0;
                        resource.type = Resource.ResourceType.TREE;
                        resource.resourceId = 0;
                    } else if (tag.Contains("door"))
                    {
                        Door door = currentobject.AddComponent<Door>();
                        //DestroyImmediate(currentobject.GetComponent<Door>());
                    } else if (tag.Contains("node"))
                    {
                        Resource resource = currentobject.AddComponent<Resource>();
                        //DestroyImmediate(currentobject.GetComponent<Resource>());
                        resource.requirement = 0;
                        resource.type = Resource.ResourceType.ROCK;
                        resource.resourceId = 0;
                    }
                }
            }
            objects.Clear();
            EditorSceneManager.SaveScene(scene);
        }

        private void CountObjects()
        {
            List<GameObject> objects = new List<GameObject>();
            objects.AddRange(UnityEngine.Object.FindObjectsOfType<GameObject>());
            Debug.Log("count: " + objects.Count);

        }

        [MenuItem("Espial/SelectMissing")]
        static void SelectMissing(MenuCommand command)
        {
            Transform[] ts = FindObjectsOfType<Transform>();
            List<GameObject> selection = new List<GameObject>();
            foreach (Transform t in ts)
            {
                Component[] cs = t.gameObject.GetComponents<Component>();
                foreach (Component c in cs)
                {
                    if (c == null)
                    {
                        selection.Add(t.gameObject);
                    }
                }
            }
            Selection.objects = selection.ToArray();
        }

    }
}
