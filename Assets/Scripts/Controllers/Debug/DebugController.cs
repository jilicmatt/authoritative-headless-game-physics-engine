﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugController : MonoBehaviour
{

    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    private float speed = 6f;

    public float turnSpeed = 100.00f;

    public float jumpRate = 0.5F;
    private float nextJump = 0.0F;
    private bool isJumpReset;

    private Vector3 moveDirection = Vector3.zero;


    // Update is called once per frame
    void Update()
    {
        CharacterController ch = GetComponent<CharacterController>();
        moveDirection = new Vector3(0, ch.isGrounded ? 0 : -gravity * Time.deltaTime, Input.GetAxis("Vertical"));
        moveDirection = transform.TransformDirection(moveDirection);
        moveDirection *= speed;
        if (ch.isGrounded)
            isJumpReset = true;
        if (Input.GetAxis("Horizontal") < -0.1f)
        {
            ch.transform.Rotate(Vector3.up, -turnSpeed * Time.deltaTime);
        }
        else if (Input.GetAxis("Horizontal") > 0.1f)
        {
            ch.transform.Rotate(Vector3.up, turnSpeed * Time.deltaTime);
        }
        if (Input.GetButton("Jump") && isJumpReset)
        {
            if (ch.isGrounded)
            {
                isJumpReset = true;
                nextJump = Time.time + jumpRate;
                moveDirection.y += jumpSpeed;
            }
            else
            {
                if (Time.time > nextJump)
                {
                    isJumpReset = false;
                    nextJump = Time.time + jumpRate;
                    moveDirection.y += jumpSpeed * 1.5f;
                }
            }
        }
        ch.Move(moveDirection * Time.deltaTime);
    }

}
