﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class DebugShipController : MonoBehaviour {

    [Header("Buoyancy")]
    public float mass = 1000f;

    public float waterLevel = 27f;

    [Range(0, 1)]
    public float dampening = 0.1f;

    public float buoyancyForceOffset = 0f;

    public Vector3 buoyancyOffset = new Vector3(0f, 0f, 0f);

    public Vector3 centerOfMass = new Vector3(0f, -15f, 0f);

    [Header("Movement")]
    public float forwardMovespeed = 10f;
    public float backwardMovespeed = 5f;
    public float steerSpeed = 0.5f;

    [Header("Tilt")]
    public float tipAngle = 10f;
    public float tipSpeed = 1f;

    Rigidbody rb;

    Vector2 input;

    Vector3 buoyancyForce;

    float velRef;

    float steer;

    float parker = 0.5f;
    float parkerLimit = 0.5f;

    public bool belowWaterLevel;

    Slope slope = Slope.None;

    Vector3 latestVel;

    void Start () {
        rb = GetComponent<Rigidbody>();
        rb.mass = mass;
    }

    void Update () {
        input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }

    void FixedUpdate () {
        Buoyancy();
        Movement();
        latestVel = rb.velocity;
    }

    void Buoyancy () {

        rb.centerOfMass = centerOfMass;

        Vector3 pos = transform.position + transform.TransformDirection(buoyancyOffset);

        if(pos.y < waterLevel + 1) {

            float dragFactor = 1f - ((pos.y - waterLevel) * 0.5f);

            if (dragFactor > 0f) {

                buoyancyForce = Vector3.up * buoyancyForceOffset - Physics.gravity * (dragFactor - rb.velocity.y * dampening);

                buoyancyForce *= rb.mass;

                if (waterLevel < pos.y && rb.velocity.y > 5f) { rb.velocity = Vector3.Scale(rb.velocity, new Vector3(1f, 0.5f, 1f)); }

                rb.AddForce(buoyancyForce);
            }
        }
    }

    void Movement () {

        if (rb.position.y > waterLevel + 1) {
            belowWaterLevel = false;
        } else {
            belowWaterLevel = true;
        }

        steer = Mathf.Lerp(steer, input.x, Time.deltaTime / 1);

        rb.angularVelocity = new Vector3(0f, input.y * steer * steerSpeed, 0f);

        Vector3 v = transform.rotation.eulerAngles;   

        if (input.y > 0) {
            if (belowWaterLevel && slope != Slope.Up) {
                rb.MovePosition(rb.position + transform.forward * (input.y * forwardMovespeed * Time.deltaTime));
                transform.rotation = Quaternion.Euler(0f, v.y, Mathf.SmoothDampAngle(v.z, input.x * -tipAngle, ref velRef, tipSpeed));
                parker = 1;
            }            
        } else if (input.y < 0) {

            if (belowWaterLevel && slope != Slope.Down) {

                rb.MovePosition(rb.position + transform.forward * (input.y * backwardMovespeed * Time.deltaTime));
                transform.rotation = Quaternion.Euler(0f, v.y, Mathf.SmoothDampAngle(v.z, input.x * -tipAngle * 0.5f, ref velRef, tipSpeed));
                parker = 1f;
            }
        } else {      
            if (belowWaterLevel) {
                transform.rotation = Quaternion.Euler(0f, v.y, Mathf.SmoothDampAngle(v.z, 0f, ref velRef, tipSpeed * 2));
                if (parker > parkerLimit) { parker -= Time.deltaTime; } else if (parker < parkerLimit) { parker = parkerLimit; }
                rb.velocity = Vector3.Scale(rb.velocity, new Vector3(parker, 1f, parker));
            }
        }
    }

    void OnCollisionStay (Collision collision) {

         if (collision.contacts.Length > 0) {

            ContactPoint contact = collision.contacts[0];

            if (Vector3.Dot(contact.normal, Vector3.up) > 0.5) {

                float s = Vector3.Dot(transform.right, (Vector3.Cross(Vector3.up, contact.normal)));

                if(s < 0) { slope = Slope.Up; } else if (s > 0) { slope = Slope.Down; } else { slope = Slope.None; }

            } else { slope = Slope.None; }
        }
    }

    void OnCollisionExit (Collision collision) { slope = Slope.None; }

    public void SetPosition (Vector3 position) { rb.position = position; }

    enum Slope { None, Up, Down }
}