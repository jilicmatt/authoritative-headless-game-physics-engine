﻿using System.Collections;
using UnityEngine;

public class KnockBackShpere : MonoBehaviour
{

    float mass = 3.0F; // defines the entity mass
    Vector3 impact = Vector3.zero;
    private CharacterController character;

    void Update()
    {
        // apply the impact force:
        if (impact.magnitude > 0.2F) character.Move(impact * Time.deltaTime);
        // consumes the impact energy each cycle:
        impact = Vector3.Lerp(impact, Vector3.zero, 5 * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        StartCoroutine(PerformKnockBack(other));
    }

    public void AddImpact(Vector3 dir, float force)
    {
        dir.Normalize();
        if (dir.y < 0) dir.y = -dir.y; // reflect down force on the ground
        impact += dir.normalized * force / mass;
    }

    IEnumerator PerformKnockBack(Collider other)
    {
        yield return new WaitForSeconds(5f);
        GameObject player = other.gameObject;
        character = player.GetComponent<CharacterController>();
        var heading = player.transform.position - transform.position;
        float dist = heading.magnitude;
        if (dist < 21)
        {
            var direction = heading / dist; // This is now the normalized direction.
            AddImpact(direction, (20f - dist) * 20f);
        }
    }

}