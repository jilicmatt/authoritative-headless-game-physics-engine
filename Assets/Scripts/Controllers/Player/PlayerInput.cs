﻿public class PlayerInput
{

    PlayerController player;

    public PlayerInput(PlayerController p) { this.player = p; }

    private float mouseY;
    private bool f;
    private float horizontal;
    private float vertical;
    private float strafing;
    private float mouseX;
    private bool jump;

    private float mouseYOld;
    private bool fOld;
    private float horizontalOld;
    private float verticalOld;
    private float strafingOld;
    private float mouseXOld;
    private bool jumpOld;

    public int key;
    public int lastProcessedKey;

    public float Horizontal {
        get { return horizontal; }

        set {
            HorizontalOld = horizontal;
            horizontal = value;
        }
    }

    public float Vertical {
        get { return vertical; }

        set {
            VerticalOld = vertical;
            vertical = value;
        }
    }

    public float Strafing {
        get { return strafing; }

        set {
            StrafingOld = strafing;
            strafing = value;
        }
    }

    public float MouseX {
        get { return mouseX; }

        set {
            MouseXOld = mouseX;
            mouseX = value;
        }
    }

    public float MouseY {
        get { return mouseY; }

        set {
            MouseYOld = mouseY;
            mouseY = value;
        }
    }

    public bool Jump {
        get { return jump; }

        set {
            JumpOld = jump;
            jump = value;
        }
    }

    public bool F {
        get { return f; }

        set {
            FOld = f;
            f = value;
        }
    }

    public float MouseYOld { get { return mouseYOld; } set { mouseYOld = value; } }

    public bool FOld { get { return fOld; } set { fOld = value; } }

    public float HorizontalOld { get { return horizontalOld; } set { horizontalOld = value; } }

    public float VerticalOld { get { return verticalOld; } set { verticalOld = value; } }

    public float StrafingOld { get { return strafingOld; } set { strafingOld = value; } }

    public float MouseXOld { get { return mouseXOld; } set { mouseXOld = value; } }

    public bool JumpOld { get { return jumpOld; } set { jumpOld = value; } }

}