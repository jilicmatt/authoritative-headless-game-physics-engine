﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PlayerController : MonoBehaviour {


    public int playerId;

    [NonSerialized]
    public float jumpSpeed = 15F;
    [NonSerialized]
    public float gravity = 18F;
    [NonSerialized]
    public float turnSpeed = 100.00f;
    [NonSerialized]
    public float speed = 6f;

    private float nextJump = 0.0F;
    private float jumpRate = 0.5F;
    private float verticalVelocity = 0;

    private bool isJumpReset;
    
    public PlayerInput input;
    public EntityController entity;
    public ShipController myShip = null;
    public CharacterController ch;
    private ShipController inRangeShip;

    private Vector3 moveDirection = Vector3.zero;

    void Start () {
        input = new PlayerInput(this);
        ch = GetComponent<CharacterController>();
        entity = transform.GetComponent<EntityController>();
    }
 
    void Update () {
        handleInteractions();
        if (myShip != null)
            return;
        moveDirection = new Vector3(input.Strafing, 0, input.Vertical);
        moveDirection = transform.TransformDirection(moveDirection);
        moveDirection *= speed;

        if (ch.isGrounded)
        {
            isJumpReset = true;
            verticalVelocity = 0;
        }
        if (input.Horizontal < -0.1f)
        {
            ch.transform.Rotate(Vector3.up, -turnSpeed * Time.deltaTime);
        } else if (input.Horizontal > 0.1f)
        {
            ch.transform.Rotate(Vector3.up, turnSpeed * Time.deltaTime);
        }
        if (input.Jump && isJumpReset)
        {
            if (ch.isGrounded)
            {
                isJumpReset = true;
                nextJump = Time.time + jumpRate;
                verticalVelocity = jumpSpeed;
            } else
            {
                if (Time.time > nextJump)
                {
                    isJumpReset = false;
                    nextJump = Time.time + jumpRate;
                    verticalVelocity = jumpSpeed * 1.5f;
                }
            }
        }

        if (transform.position.y < 0) { transform.position = new Vector3(transform.position.x, 100, transform.position.z); }

        verticalVelocity += Mathf.Clamp((ch.isGrounded ? -gravity * 10f : -gravity) * Time.deltaTime, -10, 10);

        verticalVelocity = Mathf.Clamp(verticalVelocity, -53, 10);
        moveDirection.y = verticalVelocity;
        ch.Move(moveDirection * Time.deltaTime);
        input.lastProcessedKey = input.key;
    }

    #region Ships

    void handleShipAction(bool shipInRange)
    {
        if (myShip != null)
        {
            myShip.ReleaseTheWheel();
            sendCaptainIds(-1);
            togglePrediction(true);
        } else if (shipInRange)
        {
            togglePrediction(false);
            sendCaptainIds(inRangeShip.mesh.GetComponent<EntityController>().entityId);
            inRangeShip.TakeTheWheel(this);
        } else
        {
            Server.instance.printToServerConsole("The ship is null");
        }
    }

    void OnTriggerExit(Collider collider)
    {
        ShipModelController mc = collider.GetComponent<ShipModelController>();
        if (mc != null)
        {
            ShipController ship = mc.controller;
            if (ship == inRangeShip) { inRangeShip = null; }
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        ShipModelController mc = collider.GetComponent<ShipModelController>();

        if (mc != null)
        {
            ShipController ship = mc.controller;
            inRangeShip = ship;
        }
    }

    #endregion

    #region Interactables

    private float lastInteract;
    private float interactDelay = 5f;

    private void handleInteractions()
    {
        if (Time.time - lastInteract > interactDelay)
        {
            if (input.F)
            {
                Interactable interactable = checkForInteractable();
                if (interactable != null)
                {
                    interactable.onInteract(this);
                } else
                {
                    handleShipAction(inRangeShip != null);
                }
                lastInteract = Time.time;
            }
        }
    }

    private Interactable checkForInteractable()
    {
        Vector3 position = new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z);
        Collider[] hitColliders = Physics.OverlapSphere(position, 2f);
        foreach (Collider collider in hitColliders)
        {
            Interactable interactable = collider.transform.GetComponent<Interactable>();
            if (interactable != null) { return interactable; }
        }
        return null;
    }

    #endregion

    #region Packets

    public void sendCaptainIds(int shipCaptainingEntityId)
    {
        MemoryStream stream = new MemoryStream();
        using (BinaryWriter writer = new BinaryWriter(stream))
        {
            writer.Write((byte) OutOpcode.PUSH_CAPTAIN_ID);
            writer.Write(entity.entityId);
            writer.Write(shipCaptainingEntityId);
        }
        Server.instance.Send(stream.ToArray());
    }

    public void togglePrediction(bool toggle)
    {
        MemoryStream stream = new MemoryStream();
        using (BinaryWriter writer = new BinaryWriter(stream))
        {
            writer.Write((byte) OutOpcode.TOGGLE_PREDICTION);
            writer.Write(entity.entityId);
            writer.Write(toggle);
        }
        Server.instance.Send(stream.ToArray());
    }

    public void startResourceUse()
    {
        MemoryStream stream = new MemoryStream();
        using (BinaryWriter writer = new BinaryWriter(stream))
        {
            writer.Write((byte) OutOpcode.START_RESOURCE_USE);
            writer.Write(entity.entityId);
        }
        Server.instance.Send(stream.ToArray());
    }
    #endregion

}