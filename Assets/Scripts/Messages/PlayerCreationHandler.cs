﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    class PlayerCreationHandler
    {
        Server server;

        public PlayerCreationHandler(Server server)
        {
            this.server = server;
        }

        public void onPlayerCreation(BinaryReader reader)
        {
            int entityId = reader.ReadInt32();
            
            float x = Server.readFloat(reader);
            float y = Server.readFloat(reader);
            float z = Server.readFloat(reader);

            Vector3 pos = new Vector3(x, y, z);
            Quaternion rot = Quaternion.identity;

            int playerId = reader.ReadInt32();

            GameObject player = GameObject.Instantiate(server.prefabs[0]);
            player.transform.position = pos;
            player.transform.rotation = rot;

            EntityController e = player.AddComponent<EntityController>();
            PlayerController p = player.AddComponent<PlayerController>();

            CharacterController ch = player.GetComponent<CharacterController>();

            ch.enableOverlapRecovery = true;
            ch.detectCollisions = true;

            e.entityId = entityId;
            p.playerId = playerId;

            server.world.addPlayer(p);

            Server.instance.printToServerConsole("Adding player to world");
        }

    }
}
