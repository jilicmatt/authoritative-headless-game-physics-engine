﻿using System.IO;
using UnityEngine;

namespace Assets.Scripts.handlers {

    class ShipCreationHandler {

        Server server;

        public ShipCreationHandler (Server server) { this.server = server; }

        public void onShipCreation (BinaryReader reader) {

            int entityId = reader.ReadInt32();

            float x = Server.readFloat(reader);
            float y = Server.readFloat(reader);
            float z = Server.readFloat(reader);

            Vector3 pos = new Vector3(x, y, z);

            Quaternion rot = Quaternion.identity;

            int shipId = reader.ReadInt32();

            GameObject ship = GameObject.Instantiate(server.prefabs[1]);
            ShipModelController modelController = ship.AddComponent<ShipModelController>();
            ship.transform.position = pos;
            ship.transform.rotation = rot;

            GameObject helper = GameObject.Instantiate(server.prefabs[2]);
            ShipController shipController = helper.AddComponent<ShipController>();

            Follower follower = ship.AddComponent<Follower>();
            follower.target = helper.transform;

            shipController.mesh = ship;
            modelController.controller = shipController;

            helper.transform.position = pos;
            helper.transform.rotation = rot;

            EntityController e = ship.AddComponent<EntityController>();

            e.entityId = entityId;

            server.world.addEntity(e);
        }
    }
}
