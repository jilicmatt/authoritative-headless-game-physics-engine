﻿using System.Collections;
using System.IO;
using UnityEngine;

namespace Assets.Scripts.handlers
{
    class RemoveEntityConsumer
    {
        Server server;

        public RemoveEntityConsumer(Server server)
        {
            this.server = server;
        }

        public void onRecieve(BinaryReader reader)
        {
            int entityId = reader.ReadInt32();
            server.world.removePlayer(entityId);
        }

    }
}
