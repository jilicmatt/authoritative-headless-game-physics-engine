﻿using System.IO;
using UnityEngine;

namespace Assets.Scripts
{
    class EntityMoveHandler
    {
        Server server;

        public EntityMoveHandler(Server server)
        {
            this.server = server;
        }

        public void onMove(BinaryReader reader)
        {
            int entityId = reader.ReadInt32();

            float x = Server.readFloat(reader);
            float y = Server.readFloat(reader);
            float z = Server.readFloat(reader);
            
            Vector3 pos = new Vector3(x, y, z);

            Transform entity = server.world.entities[entityId].transform;

            if (entity != null)
            {
                entity.transform.position = new Vector3(x, y, z);
            }

        }

    }
}
