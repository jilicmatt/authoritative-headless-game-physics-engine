﻿using System.IO;

namespace Assets.Scripts.handlers {

    class PlayerInputHandler {

        const int VERTICAL = 7, HORIZONTAL = 6, JUMP = 5, F = 4, STRAFING = 3, MOUSE_X = 2, MOUSE_Y = 1;

        Server server;

        public PlayerInputHandler (Server server) { this.server = server; }

        public void onPlayerInput (BinaryReader reader) {

            int playerId = reader.ReadInt32();

            PlayerController player = server.world.getPlayer(playerId);

            byte bitfield = reader.ReadByte();

            if (enabled(VERTICAL, bitfield)) { player.input.Vertical = Server.readFloat(reader); }

            if (enabled(HORIZONTAL, bitfield)) { player.input.Horizontal = Server.readFloat(reader); }

            if (enabled(JUMP, bitfield)) { player.input.Jump = reader.ReadBoolean(); }

            if (enabled(F, bitfield)) { player.input.F = reader.ReadBoolean(); }

            if (enabled(STRAFING, bitfield)) { player.input.Strafing = Server.readFloat(reader); ; }

            if (enabled(MOUSE_X, bitfield)) { player.input.MouseX = Server.readFloat(reader); ; }

            if (enabled(MOUSE_Y, bitfield)) { player.input.MouseY = Server.readFloat(reader); ; }

            player.input.key = reader.ReadInt32();
        }

        static bool enabled (int radix, byte bits) { return ((bits >> radix) & 1) == 1; }
    }
}
