﻿using System.IO;
using UnityEngine;

namespace Assets.Scripts
{
    class ApplyImpulseHandler
    {
        Server server;

        public ApplyImpulseHandler(Server server)
        {
            this.server = server;
        }

        public void onApply(BinaryReader reader)
        {
            int entityId = reader.ReadInt32();
            float x = Server.readFloat(reader);
            float y = Server.readFloat(reader);
            float z = Server.readFloat(reader);
            bool relativeToEntity = reader.ReadBoolean();

            
            GameObject entity = server.world.entities[entityId].gameObject;

            if (entity != null)
            {
                Vector3 xV = entity.transform.right * x;

                Vector3 yV = entity.transform.up * y;

                Vector3 zV = entity.transform.forward * z;

                Vector3 impulse = relativeToEntity ? xV + yV + zV : new Vector3 (x, y, z);
                ImpulseDirector.AddImpactOnGameObject(entity, impulse);
            }
        }

    }

}
