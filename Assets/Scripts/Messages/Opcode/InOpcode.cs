﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InOpcode  {
    
    CREATE_PLAYER = 0,
    CREATE_SHIP = 3,
    PLAYER_INPUT = 1,
    APPLY_IMPULSE = 20,
    REMOVE_ENTITY = 223,
    ENTITY_MOVE = 5

}
