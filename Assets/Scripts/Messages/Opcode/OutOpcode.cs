﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum OutOpcode  {

    PUSH_ENTITY_LOCATION = 0,
    SCENE_LOADED = 1,
    PUSH_CAPTAIN_ID = 77,
    TOGGLE_PREDICTION = 78,
    START_RESOURCE_USE = 55
}
