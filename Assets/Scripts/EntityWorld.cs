﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EntityWorld : MonoBehaviour {
    
    private Server server;

    void Awake()
    {
        server = Server.instance;
    }

    // Use this for initialization
    void Start () {
        entities = new Dictionary<int, EntityController>();
        players = new ConcurrentDictionary<int, PlayerController>();
        LoadScene("Location1");
    }

    // Update is called once per frame
    void Update () {
        foreach (EntityController e in entities.Values) {
            if (e.lastPosition != e.transform.position) { pushEntityLocation(e); }        
        }
    }

    public Dictionary<int, EntityController> entities;
    public ConcurrentDictionary<int, PlayerController> players;

    #region Entity Management

    public void addEntity(EntityController e)
    {
        entities[e.entityId] = e;
    }

    public void removeEntity(int id)
    {
        EntityController e = entities[id];
        GameObject.Destroy(e.gameObject);
        entities.Remove(id);
    }

    public EntityController getEntity(int id) { return entities[id]; }

    GameObject addEntity(int id, Vector3 position, Quaternion orientation)
    {
        GameObject it = GameObject.Instantiate(Server.instance.prefabs[0]);
        it.transform.SetPositionAndRotation(position, orientation);
        it.name = "Entity" + id;
        it.AddComponent<PlayerController>();
        return it;
    }

    #endregion

    #region Player Management
    
    public void addPlayer(PlayerController p)
    {
        players[p.playerId] = p;
        addEntity(p.gameObject.GetComponent<EntityController>());
    }

    public void removePlayer(int id)
    {
        PlayerController p = players[id];
        PlayerController tmp;
        players.TryRemove(id, out tmp);
        removeEntity(p.gameObject.GetComponent<EntityController>().entityId);
    }

    public PlayerController getPlayer(int id) { return players[id]; }

    #endregion

    #region Scene Management

    public void LoadScene(string sceneName) { StartCoroutine(LoadSceneAsync(sceneName)); }

    public void UnloadScene(string name) { SceneManager.UnloadSceneAsync(name); }

    private IEnumerator LoadSceneAsync(string sceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        while (!asyncLoad.isDone)
        {
            PushSceneLoaded(sceneName);
            yield return null;
        }
    }

    #endregion

    #region Packets

    private void pushEntityLocation(EntityController cont)
    {
        MemoryStream stream = new MemoryStream();
        using (BinaryWriter writer = new BinaryWriter(stream))
        {
            writer.Write((byte) OutOpcode.PUSH_ENTITY_LOCATION);
            writer.Write((int) cont.entityId);

            Vector3 pos = cont.transform.position;
            Quaternion rot = cont.transform.rotation;

            writer.Write((float) (pos.x));
            writer.Write((float) (pos.y));
            writer.Write((float) (pos.z));

            writer.Write((float) (rot.x));
            writer.Write((float) (rot.y));
            writer.Write((float) (rot.z));
            writer.Write((float) (rot.w));

            PlayerController controller = cont.gameObject.GetComponent<PlayerController>();
            if (controller != null)
            {
                writer.Write(controller.ch.isGrounded);
                writer.Write(controller.input.lastProcessedKey);
            }

        }
        server.Send(stream.ToArray());
    }

    private void PushSceneLoaded(string sceneName)
    {
        MemoryStream stream = new MemoryStream();
        using (BinaryWriter writer = new BinaryWriter(stream))
        {
            writer.Write((byte) OutOpcode.SCENE_LOADED);
            writer.Write(SceneManager.GetSceneByName(sceneName).buildIndex);
        }
        server.Send(stream.ToArray());
    }

    #endregion

}
