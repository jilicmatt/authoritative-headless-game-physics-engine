﻿using Assets.Scripts;
using Assets.Scripts.handlers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

public class Server : MonoBehaviour
{
    [SerializeField]
    public EntityWorld world;
    public static Server instance;
    
    [NonSerialized]
    public string token;
    
    private bool shouldRun;
    private int remotePort;
    private string remoteHost;
    private TcpClient client;
    private IPEndPoint server_ip;
    private Thread receiveThread;

    private StreamWriter outStream;

    private float timeSinceLastMessageFromServer = 0;
    private float LIMIT = 10;

    void Awake()
    {
        instance = this;
    }
    
    // Use this for initialization
    void Start()
    {
        string os = System.Environment.OSVersion.ToString();

        if (os.Contains("windows") || os.Contains("Windows"))
        {
            Debug.Log("Running in development mode with a 10000s death delay because found windows OS");
            LIMIT = 1000f;
        } else {
            Debug.Log("Running in server mode with short death time because found non windows OS");
            LIMIT = 10f;
        }

        remoteHost = System.Environment.GetEnvironmentVariable("GAME_SERVER_HOST");
        remotePort = int.Parse(System.Environment.GetEnvironmentVariable("GAME_SERVER_PORT"));
        token = System.Environment.GetEnvironmentVariable("TOKEN");

        printToServerConsole(token);

        client = new TcpClient();
        client.Connect(remoteHost, remotePort);

        printToServerConsole("Connected to server");

        var stream = client.GetStream();

        outStream = new StreamWriter(stream);
        SendInitialPacket(new byte[] { 1 }); // initial packet, tells Game Server that it is ready

        ThreadPool.QueueUserWorkItem((hello) =>
        {
            printToServerConsole("Starting receipt thread");
            var reader = new StreamReader(stream);
            while (shouldRun)
            {
                printToServerConsole("Awaiting input from master");
                string input = reader.ReadLine();
                printToServerConsole("Input received from master " + input);
                timeSinceLastMessageFromServer = 0;
                try
                {
                    string[] spl = input.Split(' ');
                    printToServerConsole("Split into: " + string.Join(",", spl));
                    byte[] args = new byte[spl.Length];
                    for (int x = 0; x < spl.Length; x++)
                    {
                        args[x] = (byte)sbyte.Parse(spl[x]);
                    }
                    MemoryStream ms = new MemoryStream(args);
                    BinaryReader br = new BinaryReader(ms);
                    int opcode = br.ReadByte();
                    if (handlers.ContainsKey(opcode))
                    {
                        printToServerConsole("Handler found");
                        handlers[opcode](br);
                    }
                    else
                    {
                        printToServerConsole("Unknown opcode " + opcode);
                    }
                }
                catch (Exception e)
                {
                    printToServerConsole(e.ToString());
                }
            }
        });
        shouldRun = true;
        initHandlers();
    }

    // Update is called once per frame
    void Update()
    {
        timeSinceLastMessageFromServer += Time.deltaTime;
        if (timeSinceLastMessageFromServer > LIMIT)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
    }

    void OnDisable()
    {
        shouldRun = false;
        client.Close();
    }

    #region Handlers
    private void initHandlers()
    {
        handlers[(int) InOpcode.CREATE_PLAYER] = new PlayerCreationHandler(this).onPlayerCreation;
        handlers[(int) InOpcode.PLAYER_INPUT] = new PlayerInputHandler(this).onPlayerInput;
        handlers[(int) InOpcode.CREATE_SHIP] = new ShipCreationHandler(this).onShipCreation;
        handlers[(int) InOpcode.ENTITY_MOVE] = new EntityMoveHandler(this).onMove;
        handlers[(int) InOpcode.APPLY_IMPULSE] = new ApplyImpulseHandler(this).onApply;
        handlers[(int) InOpcode.REMOVE_ENTITY] = new RemoveEntityConsumer(this).onRecieve;
    }
    #endregion

    #region Packet

    private void SendInitialPacket(byte[] v)
    {
        Send(v);
    }

    public static float readFloat(BinaryReader reader)
    {
        byte[] tmp = new byte[4];
        int count = reader.Read(tmp, 0, 4);
        Array.Reverse(tmp);
        return System.BitConverter.ToSingle(tmp, 0);
    }

    public void printToServerConsole(string log)
    {
        MemoryStream stream = new MemoryStream();
        using (BinaryWriter writer = new BinaryWriter(stream))
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(log);

            writer.Write((byte) 125);
            writer.Write((int) bytes.Length);
            writer.Write(bytes);
        }
        Send(stream.ToArray());
    }

    public void Send(byte[] content)
    {
        String str = token;
        foreach (byte b in content)
        {
            str += (sbyte) b + " ";
        }
        outStream.WriteLine(str.Trim());
        outStream.Flush();
    }
    
    public static IPEndPoint GetIPEndPointFromHostName(string hostName, int port, bool throwIfMoreThanOneIP)
    {
        var addresses = System.Net.Dns.GetHostAddresses(hostName);
        if (addresses.Length == 0)
        {
            throw new ArgumentException(
                "Unable to retrieve address from specified host name.",
                "hostName"
            );
        } else if (throwIfMoreThanOneIP && addresses.Length > 1)
        {
            throw new ArgumentException(
                "There is more that one IP address to the specified host.",
                "hostName"
            );
        }
        return new IPEndPoint(addresses[0], port); // Port gets validated here.
    }

    #endregion

    private Dictionary<int, Action<BinaryReader>> handlers = new Dictionary<int, Action<BinaryReader>>();
    public List<GameObject> prefabs;

}
