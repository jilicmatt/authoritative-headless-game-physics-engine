﻿using System.IO;
using UnityEngine;

public class Door : Interactable
{
    Quaternion desiredRotation;
    Quaternion originalRotation;

    bool open;

    void Start()
    {
        originalRotation = transform.rotation;
        desiredRotation = Quaternion.identity;
    }

    public override void onInteract(PlayerController player)
    {
        if (open) {
            open = false;
            desiredRotation = originalRotation;
        } else {
            open = true;
            desiredRotation = Quaternion.Euler(new Vector3(transform.rotation.x, transform.rotation.y + 90, transform.rotation.z));
        }

        PushRotation(player.entity);
    }

    void Update()
    {
        if (transform.rotation != desiredRotation)
            this.transform.rotation = Quaternion.Lerp(transform.rotation, desiredRotation, Time.deltaTime * 2);
    }

    void PushRotation(EntityController entity)
    {
        MemoryStream stream = new MemoryStream();
        using (BinaryWriter writer = new BinaryWriter(stream))
        {
            writer.Write((byte) 80);//opcode
            writer.Write((int) entity.entityId);//playerid
            writer.Write((float) transform.position.x);//
            writer.Write((float) transform.position.y);//
            writer.Write((float) transform.position.z);//
        }
        Server.instance.Send(stream.ToArray());
    }

}
