﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Resource : Interactable {

    [Header("Requirement")]
    public int requirement;

    [Header("Resource type")]
    public ResourceType type;

    [Header("Id of resource (e.g. oak)")]
    public int resourceId;

    IEnumerator RespawnTimer(int timer, Collider collider)
    {
        yield return new WaitForSeconds(timer);
        collider.enabled = true;
    }

    IEnumerator AttemptGather(PlayerController player)
    {
        Collider collider = gameObject.GetComponent<Collider>();
        int attemptInSeconds = (int) UnityEngine.Random.Range(5f, 10f);
        //player.FreezeInput = true;
        player.startResourceUse();//tell server that chopping started
        var lookPos = player.transform.position - transform.position;
        lookPos.y = 0;
        var rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 10f);
        yield return new WaitForSeconds(attemptInSeconds);
        //player.FreezeInput = false;
        collider.enabled = false;
        int respawnTimer = (int) UnityEngine.Random.Range(5f, 10f);
        PushRespawn(player.entity.entityId, respawnTimer);
        StartCoroutine(RespawnTimer(respawnTimer, collider));
    }
    
    public const int PUSH_RESPAWN_PACKET = 5;

    void PushRespawn(int entityId, int timer)
    {
        MemoryStream stream = new MemoryStream();
        using (BinaryWriter writer = new BinaryWriter(stream))
        {
            writer.Write((byte) PUSH_RESPAWN_PACKET);
            writer.Write(entityId);//playerid
            writer.Write((float) transform.position.x);//
            writer.Write((float) transform.position.y);//
            writer.Write((float) transform.position.z);//
            writer.Write((int)type);//tree/rock etc
            writer.Write(resourceId);//oak, iron etc
            writer.Write(timer);//respawn time
        }
        Server.instance.Send(stream.ToArray());
    }

    public override void onInteract(PlayerController player)
    {
        StartCoroutine(AttemptGather(player));
    }

    public enum ResourceType : int
    {
        TREE,
        ROCK
    }
}
